# Copyright 2023 Boran Hao, Nasser Hashemi, Dima Kozakov

#

# Licensed under the Apache License, Version 2.0 (the "License");

# you may not use this file except in compliance with the License.

# You may obtain a copy of the License at

#

#      http://www.apache.org/licenses/LICENSE-2.0

#

# Unless required by applicable law or agreed to in writing, software

# distributed under the License is distributed on an "AS IS" BASIS,

# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

# See the License for the specific language governing permissions and

# limitations under the License.


import torch
from transformers import (
    AutoTokenizer,
    Trainer,
    TrainingArguments,
    AutoModelForSequenceClassification,
    BertForSequenceClassification,
)
from torch.utils.data import Dataset
import os
import pandas as pd
import requests
from tqdm.auto import tqdm
import numpy as np
from sklearn.metrics import (
    accuracy_score,
    precision_recall_fscore_support,
    roc_auc_score,
)
import re
import esm
import csv
import scipy


os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"

task_name = "13mAllSingle_prot_bert_esm_5e-5_ep1selection15pad"


model_name = "prot_bert_bfd/"
max_length = 128
# sep_token = '/'
sep_token = "<unk>"

_, alphabet = esm.pretrained.esm2_t33_650M_UR50D()
print(_.num_layers)
print(_.embed_dim)


train_filename = "13m_AllSingle_TrainSplit.csv"
valid_filename = "13m_AllSingle_ValSplit.csv"
test_filenames = ["13m_AllSingle_TrainSplit.csv"]


class DeepLocDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(
        self, split="train", tokenizer_name="Rostlab/prot_bert_bfd", max_length=1024
    ):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """

        self.datasetFolderPath = ""
        self.trainFilePath = os.path.join(self.datasetFolderPath, train_filename)
        self.validFilePath = os.path.join(self.datasetFolderPath, valid_filename)

        self.tokenizer = AutoTokenizer.from_pretrained(
            tokenizer_name, do_lower_case=False
        )

        self.alphabet = alphabet
        self.batch_converter = self.alphabet.get_batch_converter()

        if split == "train":
            self.seqs, self.labels = self.load_dataset(
                self.trainFilePath, IsEp1TrainSet=False
            )
            print(self.seqs[:15])
            print(self.labels[:15])
        elif split == "valid":
            self.seqs, self.labels = self.load_dataset(self.validFilePath)
        else:
            self.seqs, self.labels = self.load_dataset(split)

        self.max_length = max_length

    def load_dataset(self, path, IsEp1TrainSet=False):
        df = pd.read_csv(path)

        if IsEp1TrainSet == True:  # for epoch1 only use these peptides
            df = df[df["num_mhc"] == 1]

        df = df.loc[df["binding_label"].isin([0, 1])]
        self.labels_dic = {0: "not_binded", 1: "binded"}

        df["labels"] = np.where(df["binding_label"] == 1, 1, 0)

        seq1 = list(df["MHC_seq"])
        seq2 = list(df["peptide_seq"])

        seq = [
            " ".join(seq1[i].upper())
            + " "
            + sep_token
            + " "
            + " ".join(seq2[i].upper())
            for i in range(len(seq1))
        ]  # .upper(): there were some strange lower cased AA, hopefully not too many since in old esm1b they would be recognized as <unk>
        if task_name == "paccman":
            seq = ["".join(s.split()) for s in seq]
        label = list(df["labels"])

        assert len(seq) == len(label)
        return seq, label

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        seq = self.seqs[idx]
        seq = re.sub(r"[UZOB]", "X", seq)
        seq_ids = self.tokenizer(
            seq, truncation=True, padding="max_length", max_length=self.max_length
        )
        batch_labels, batch_strs, batch_tokens = self.batch_converter(
            [("protein1", "".join(seq.split()))]
        )

        seq_len = batch_tokens[0].shape[0]

        sample = {key: torch.tensor(val) for key, val in seq_ids.items()}
        sample["labels"] = torch.tensor(self.labels[idx])

        sample["seq_len"] = seq_len

        sample["batch_tokens"] = torch.nn.functional.pad(
            batch_tokens[0], (0, 52 - seq_len), mode="constant", value=1
        )  # the 13m dataset has len8-15, so we pad everything to 34+15+3=52, instead of 47 in len10 before

        sample["g"] = (
            torch.tensor(self.labels[idx]) + 3
        )  # an example of the g we would add

        return sample


train_dataset = DeepLocDataset(
    split="train", tokenizer_name=model_name, max_length=max_length
)
val_dataset = DeepLocDataset(
    split="valid", tokenizer_name=model_name, max_length=max_length
)


def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    prodict_prob = scipy.special.softmax(pred.predictions, axis=1)[:, 1]
    precision, recall, f1, _ = precision_recall_fscore_support(
        labels, preds, average="binary"
    )
    acc = accuracy_score(labels, preds)
    auc = roc_auc_score(labels, prodict_prob)
    # compute PPV like Dima's group
    prodict_prob_list = list(prodict_prob)
    labels_list = list(labels)
    top_ind = sorted(
        range(len(prodict_prob_list)), key=lambda k: prodict_prob_list[k], reverse=True
    )
    top_ind = top_ind[
        : int(np.sum(labels_list))
    ]  # using the top (# gt positive sample) confident predictions to compute PPV
    top_pred_gt_labels = [labels[i] for i in top_ind]
    print(top_pred_gt_labels)
    ppv = np.sum(top_pred_gt_labels) / len(top_pred_gt_labels)

    return {
        "accuracy": acc,
        "f1": f1,
        "precision": precision,
        "recall": recall,
        "auc": auc,
        "PPV": ppv,
    }


from torch.nn import CrossEntropyLoss, MSELoss, LSTM
from transformers.modeling_outputs import SequenceClassifierOutput
from esm.pretrained import load_model_and_alphabet_local


class our_esm(BertForSequenceClassification):
    def __init__(self, config):
        super().__init__(config)
        self.classifier = torch.nn.Linear(_.embed_dim, config.num_labels)
        self.esm, alphabet = esm.pretrained.esm2_t33_650M_UR50D()

        for param in self.bert.parameters():
            param.requires_grad = False  # the way to freeze the bert layer!

        for param in self.esm.parameters():
            param.requires_grad = True  # the way to freeze the bert layer!

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        labels=None,
        output_attentions=None,
        output_hidden_states=None,
        return_dict=None,
        g=None,  # pass the extra parameters to the network!
        batch_tokens=None,
        seq_len=None,  # pass seq len to truncate seqs
    ):
        r"""
        labels (:obj:`torch.LongTensor` of shape :obj:`(batch_size,)`, `optional`):
            Labels for computing the sequence classification/regression loss. Indices should be in :obj:`[0, ...,
            config.num_labels - 1]`. If :obj:`config.num_labels == 1` a regression loss is computed (Mean-Square loss),
            If :obj:`config.num_labels > 1` a classification loss is computed (Cross-Entropy).
        """
        return_dict = (
            return_dict if return_dict is not None else self.config.use_return_dict
        )

        for param in self.esm.parameters():
            param.requires_grad = True  # the way to freeze the bert layer!

        outputs2 = self.esm(batch_tokens, [_.num_layers], False)

        hn = outputs2["representations"][_.num_layers][
            :, 0, :
        ]  # in 13m data since the seq len difference is larger, we use the [CLS] token instead of average suggested by Facebook

        pooled_output = self.dropout(hn)
        logits = self.classifier(pooled_output)

        loss = None
        if labels is not None:
            if self.num_labels == 1:
                #  We are doing regression
                loss_fct = MSELoss()
                loss = loss_fct(logits.view(-1), labels.view(-1))
            else:
                loss_fct = CrossEntropyLoss()
                loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))

        if not return_dict:
            output = (logits,) + outputs2[2:]
            return ((loss,) + output) if loss is not None else output2

        return SequenceClassifierOutput(
            loss=loss,
            logits=logits,
        )


def model_init():

    m = our_esm.from_pretrained(model_name)
    mm, _ = esm.pretrained.esm2_t33_650M_UR50D()
    m.esm = mm
    return m

    # return our_esm.from_pretrained('results_13mAllSingle_prot_bert_esm_5e-5_ep1selection15pad/checkpoint-15194/')


training_args = TrainingArguments(
    output_dir="./results_" + task_name,  # output directory
    num_train_epochs=1,  # total number of training epochs # 1st epoch of selection strategy
    per_device_train_batch_size=128,  # batch size per device during training
    per_device_eval_batch_size=128,  # batch size for evaluation
    # warmup_steps=1000,               # number of warmup steps for learning rate scheduler
    learning_rate=5e-5,
    warmup_ratio=0.1,
    weight_decay=0.01,  # strength of weight decay
    logging_dir="./logs",  # directory for storing logs
    logging_steps=200,  # How often to print logs
    do_train=True,  # Perform training
    do_eval=True,  # Perform evaluation
    evaluation_strategy="epoch",  # evalute after eachh epoch
    gradient_accumulation_steps=1,  # total number of steps before back propagation
    # fp16=True,                       # Use mixed precision
    # fp16_opt_level="02",             # mixed precision mode
    run_name="ProBert-BFD-MS",  # experiment name
    seed=4,  # 3,                           # Seed for experiment reproducibility 3x3 # change seed for 3rd epoch
    save_total_limit=20,
    # save_steps=50,
    save_strategy="epoch",  # "steps"
    # lr_scheduler_type='constant',
)


trainer = Trainer(
    model_init=model_init,  # the instantiated ?? Transformers model to be trained
    args=training_args,  # training arguments, defined above
    train_dataset=train_dataset,  # training dataset
    eval_dataset=val_dataset,  # evaluation dataset
    compute_metrics=compute_metrics,  # evaluation metrics
)


trainer.train()


for test_filename in test_filenames:

    test_dataset = DeepLocDataset(
        split=test_filename, tokenizer_name=model_name, max_length=max_length
    )

    predictions, label_ids, metrics = trainer.predict(test_dataset)
    print(metrics)
    print(predictions)

    import scipy

    predicted_prob = scipy.special.softmax(predictions, axis=1)[:, 1]
    print(predicted_prob)

    test_data = [
        item for item in csv.reader(open(test_filename, "r", encoding="utf-8"))
    ]

    out = open(
        test_filename[:-4] + "_" + task_name + "_output.csv",
        "a",
        newline="",
        encoding="utf-8",
    )
    csv_write = csv.writer(out, dialect="excel")
    csv_write.writerow(test_data[0] + [task_name])

    test_data = test_data[1:]

    for i in range(len(test_data)):
        data_line = test_data[i]
        pred_prob = predicted_prob[i]
        data_line.append(pred_prob)
        csv_write.writerow(data_line)

    out.close()
